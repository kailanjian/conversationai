﻿using Proxem.Antelope.Tagging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Proxem.Antelope.Parsing;
using Proxem.Antelope.Tools;
using Proxem.Antelope;

namespace ConversationAI
{
    class Sentence
    {
        //Enums
        public enum SentenceTypes { Unknown, Command, Question, Statement, Conditional };
        //CONSTANTS

        // sentence types
        public const int UNKNOWN = 0;
        public const int COMMAND = 1;
        public const int QUESTION = 2;
        public const int STATEMENT = 3;

        //punctuation
        public const int NONE = 0;
        public const int PERIOD = 1;
        public const int QUESTION_MARK = 2;
        public const int EXCLAMATION_MARK = 3;

        //Class Variables
        
        //Properties
        public string Text { get; set; }
        public IList<Word> Words { get; set; }
        public IAnalysis Analysis { get; set; }
        public IList<IChunk> Chunks { get; set; }
        public IList<IDeepDependency> Dependencies { get; set; }
        public ISyntacticNode AntelopeRootNode { get; set; }
        public NodeTree NodeTree { get; set; }
        public IList<IToken> Tokens { get; set; }
        public SentenceTypes Type { get; set; }

        public Sentence(string text, IList<Word> words, IAnalysis analysis, IList<IChunk> chunks, 
            IList<IDeepDependency> dependencies, ISyntacticNode rootNode, IList<IToken> tokens, SentenceTypes type)
        {
            Type = type;
            Words = words;
            Analysis = analysis;
            Dependencies = dependencies;
            AntelopeRootNode = rootNode;
            Tokens = tokens;
            Chunks = chunks;
            NodeTree = new NodeTree(rootNode);
        }
        public IDeepDependency GetDependencyOfType(DeepDependencyType dependencyType, IList<IDeepDependency> dependencies)
        {
            IDeepDependency matchedDependency = null;
            foreach (IDeepDependency dependency in dependencies)
            {
                if (dependency.DepType == dependencyType)
                {
                    matchedDependency = dependency;
                }
            }
            return matchedDependency;
        }
    }
}
