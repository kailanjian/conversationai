﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Proxem.Antelope.Parsing;
using Proxem.Antelope.Tagging;

namespace ConversationAI
{
    class Node
    {
        // Class Variables
    
        //Property
        // Auto-Implemented properties
        public Node Parent { get; set; }
        public List<Node> Children { get; set; }
        public Word Leaf { get; set; }
        public SyntacticNodeType Type { get; set; }

        //Constructor
        public Node(ISyntacticNode node) : this()
        {
            if (node.Leaf != null)
            {
                Leaf = new Word(node.Leaf);
            }
            Type = node.Type;
        }

        private Node()
        {
            Children = new List<Node>();
        }
    }
}
