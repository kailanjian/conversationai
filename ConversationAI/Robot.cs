﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConversationAI
{
    public abstract class Robot
    {

        // Properties
        // Auto-Implemented Properties
        public string Name { get; set; }
        public int RobotType { get; set; }
        public int Assertiveness { get; set; }
        public int Factuality { get; set; }
        public int Emotion { get; set; }
        public int Organization { get; set; }
        public Robot(string name, int robotType, int assertiveness, int factuality, int emotion, int organization)
        {
            Name = name;
            RobotType = robotType;
            Assertiveness = assertiveness;
            Factuality = factuality;
            Emotion = emotion;
            Organization = organization;
        }
        public abstract void ProcessInput(string input);

    }
}
