﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ConversationAI.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "12.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("..\\..\\..\\data\\Proxem.Lexicon.dat")]
        public string LexiconFile {
            get {
                return ((string)(this["LexiconFile"]));
            }
            set {
                this["LexiconFile"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("..\\..\\..\\data\\BrillTaggerLexicon.txt")]
        public string SimpleTaggerFile {
            get {
                return ((string)(this["SimpleTaggerFile"]));
            }
            set {
                this["SimpleTaggerFile"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("..\\..\\..\\bin\\Tagger.dll")]
        public string TaggerDLL {
            get {
                return ((string)(this["TaggerDLL"]));
            }
            set {
                this["TaggerDLL"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("..\\..\\..\\data\\SSTagger\\model.bidir.%d")]
        public string SSTaggerModelFile {
            get {
                return ((string)(this["SSTaggerModelFile"]));
            }
            set {
                this["SSTaggerModelFile"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("..\\..\\..\\data\\StanfordParser\\wsjPCFG.txt")]
        public string StanfordParserFile {
            get {
                return ((string)(this["StanfordParserFile"]));
            }
            set {
                this["StanfordParserFile"] = value;
            }
        }
    }
}
