﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ConversationAI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Indexes for the Combobox We don't want to hardcode these so we can add more
        private const int BUTLER_COMBOBOXITEM = 0;

        private bool nameChanged = false;
        int robotType = 0;        

        public MainWindow()
        {
            InitializeComponent();
            //selectionComboBox.Items.Add()
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Slider slider = sender as Slider;
            //slider
            if(slider.Value > 5)
            {
               if(slider.Name.Equals(sliderA.Name))
               {
                   buttonA2.IsChecked = true;
               }
               else if(slider.Name.Equals(sliderB.Name))
               {
                   buttonB2.IsChecked = true;
               }
               else if(slider.Name.Equals(sliderC.Name))
               {
                   buttonC2.IsChecked = true;
               }
               else if(slider.Name.Equals(sliderD.Name))
               {
                   buttonD2.IsChecked = true;
               }
            }
            if (slider.Value < 5)
            {
                if (slider.Name.Equals(sliderA.Name))
                {
                    buttonA1.IsChecked = true;
                }
                else if (slider.Name.Equals(sliderB.Name))
                {
                    buttonB1.IsChecked = true;
                }
                else if (slider.Name.Equals(sliderC.Name))
                {
                    buttonC1.IsChecked = true;
                }
                else if (slider.Name.Equals(sliderD.Name))
                {
                    buttonD1.IsChecked = true;
                }
            }
        }
        private void button_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton button = sender as RadioButton;
            if      (button.Name.Equals(buttonA1.Name))
                sliderA.Value = -30;
            else if (button.Name.Equals(buttonA2.Name))
                sliderA.Value = 30;
            else if (button.Name.Equals(buttonB1.Name))
                sliderB.Value = -30;
            else if (button.Name.Equals(buttonB2.Name))
                sliderB.Value = 30;
            else if (button.Name.Equals(buttonC1.Name))
                sliderC.Value = -30;
            else if (button.Name.Equals(buttonC2.Name))
                sliderC.Value = 30;
            else if (button.Name.Equals(buttonD1.Name))
                sliderD.Value = -30;
            else if (button.Name.Equals(buttonD2.Name))
                sliderD.Value = 30;
        }

        private void TextBox_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            TextBox textBox = sender as TextBox;

            if(!nameChanged)
            {
                textBox.Text = "";
                textBox.Foreground = Brushes.Black;
                nameChanged = true;
            }
        }

        private void TextBox_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            TextBox textBox = sender as TextBox;

            if(textBox.Text.Equals(""))
            {
                nameChanged = false;
                textBox.Foreground = Brushes.Gray;
                textBox.Text = "Name";
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(!nameChanged)
                nameTextBox.Text = "No Name";
            Main main = new Main(nameTextBox.Text, robotType, (int)sliderA.Value, (int)sliderB.Value, (int)sliderC.Value, (int)sliderD.Value, this);


        }
    }
}
