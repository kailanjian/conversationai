﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConversationAI
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class ChatWindow : Window
    {
        private Robot robot;
        private int numLines=0;
        
        public ChatWindow(Robot robot)
        {
            InitializeComponent();
            this.robot = robot;
            
        }
        public void AddLineToChatBox(string messenger, string message)
        {
            Run messengerName = new Run(messenger);
            messengerName.Foreground = Brushes.Red;
            chatBox.Inlines.Add(messengerName);
            chatBox.Inlines.Add("> " + message);
            //chatBox.Text += "\n"+ messenger + "> " + message;
            numLines += 1;
            if(numLines > 8) // to keep the current message at the center of the screen, automatically scroll down a line every new message
            {
                chatBox.Height += 16; // 16 is the height of a row of text
                scrollBar.LineDown();
            }
            chatBox.Inlines.Add("\n");
        }
        public void AddLineToChatBox(string messenger, string message, SolidColorBrush color)
        {
            // Make the name of the sender a seperate Run so we can access and modify the color later on.
            Run messengerName = new Run(messenger);
            messengerName.Foreground = color;
            // This sets up the format: Name> Message
            chatBox.Inlines.Add(messengerName);
            chatBox.Inlines.Add("> " + message);

            //chatBox.Text += "\n"+ messenger + "> " + message;
            numLines += 1;
            if (numLines > 8) // to keep the current message at the center of the screen, automatically scroll down a line every new message
            {
                chatBox.Height += 16; // 16 is the height of a row of text
                scrollBar.LineDown();
            }
            // Add a newline after message is sent so the next message starts on the next line.
            chatBox.Inlines.Add("\n");
        }
        public Run AddRunWithInstance()
        {
            Run run = new Run();
            run.Text = "Arghh";
            chatBox.Inlines.Add(run);
            chatBox.Inlines.Add("\n");
            return run;
        }
        public void DebugMessage(string message)
        {
            if (Main.DEBUG)
            {
                AddLineToChatBox("[Debug]", message, Brushes.DimGray);
            }
        }
        public void SystemMessage(string message)
        {
            AddLineToChatBox("[System]", message, Brushes.MidnightBlue);
        }
        public void Send()
        {

            string input = messageBox.Text;
            if (input.Equals("Write your message here...") || input.Equals(""))
            {
                //User did not type anything, do not put it up.
            }
            else 
            {
                AddLineToChatBox("User", input);
                messageBox.Text = "";
                robot.ProcessInput(input);
            }
        }

        private void sendButton_Click(object sender, RoutedEventArgs e)
        {
            Send();
            
        }

        private void messageBox_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            messageBox.Text = "";
            messageBox.Foreground = Brushes.Black;

        }

        private void messageBox_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if(messageBox.Text.Equals(""))
            {
                messageBox.Text = "Write your message here...";
                messageBox.Foreground = Brushes.Gray;
            }
        }
        
    }
}
