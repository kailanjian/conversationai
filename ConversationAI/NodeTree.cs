﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Proxem.Antelope.Parsing;

namespace ConversationAI
{
    class NodeTree
    {
        // Class Variables

        //Properties
        public Node RootNode { get; set; }      
    
        //Constructor
        public NodeTree()
        {
            
        }

        public NodeTree(ISyntacticNode antelopeRootNode)
        {
            RootNode = new Node(antelopeRootNode);
            CopyTree(antelopeRootNode, RootNode);
        }
        
        //Methods
        /// <summary>
        /// Copies the contents of an Antelope ISyntacticNode tree to my custom Node tree
        /// </summary>
        /// <param name="antelopeNode">The Antelope ISyntacticNode tree root node to be copied</param>
        /// <param name="node">Empty Node to recieve contents of the Antelope node</param>
        public void CopyTree(ISyntacticNode antelopeNode, Node node)
        {
            if (antelopeNode.Children != null)
            {
                foreach (ISyntacticNode antelopeChildNode in antelopeNode.Children)
                {
                    Node childNode = new Node(antelopeChildNode);
                    node.Children.Add(childNode);
                    childNode.Parent = node;
                    CopyTree(antelopeChildNode, childNode);
                }
            }
        }
    }
}
