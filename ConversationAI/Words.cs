﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConversationAI
{
    class Words
    {
        //Encoding, 1000*x + word index where x = type of word based on the standard in Word.cs
        //UNKNOWN = 0 NOUN = 1 PRONOUN = 2 ADJECTIVE = 3 VERB = 4 ADVERB = 5 PREPOSITION = 6 CONJUNCTION = 7 INTERJECTION = 8

        public const int UNKNOWN = 0;
        //Nouns 1000+
        public const int FILE = 1001; //Computer file.
        //Pronouns 2000+ also, because you didn't learn enough in school:http://grammar.ccc.commnet.edu/grammar/pronouns1.htm
        public const int I = 2001; // A personal pronoun
        //Adjectives 3000+

        //Verbs 4000
        public const int OPEN = 4001; //As in "open a file". opening programs is the same as running them so that qualifies as well
        //Adverb 5000+

        //Preposition 6000+
        public const int TO = 6001;
        public const int OF = 6002;
        //Conjunction 7000+

        //Interjection 8000+

    }
}
