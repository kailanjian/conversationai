﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConversationAI
{
    class ProgressManager
    {

        public int ProgressPercentage { get; set; }
        public string ProgressInformation { get; set; }
        public ProgressManager()
        {
            
        }
        public void WipeProgress()
        {
            ProgressPercentage = 0;
            ProgressInformation = "";
        }

    }
}
