﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Proxem.Antelope.Lexicon;
using Proxem.Antelope;
using Proxem.Antelope.Tagging;
using Proxem.Antelope.Parsing;
using Proxem.Antelope.Tools;
using Proxem.Antelope.Predicates;
using Proxem.Antelope.LinkGrammar;

namespace ConversationAI
{
    class Translator
    {
        private Main main;
        private Dictionary<string, Word> stringWordDictionary = new Dictionary<string, Word>();
        private ITagger tagger;
        private IChunker chunker;
        private IParser linkGrammarParser;
        private IParser stanfordParser;
        private ILexicon lexicon;
        public enum ParserType { LinkGrammar, Stanford };
        public Translator(Main main)
        {
            this.main = main;
        }

        // The Setup is broken into methods, LoadTagger, LoadChunker, LoadParser, LoadLexicon so we can Report Progress after each is loaded.
        // This sucks though because I have to make sure all of them are run. If only there was a better way...

        //public void SetupTools(ProgressManager progressManager)
        //{
        //    // Create the tools of the class (AKA: Time consuming tasks to be run seperately in a different thread)

        //    // There are two tagger types:
        //    // 1) The following is the simple tagger
        //    tagger = new SimpleTagger(Properties.Settings.Default.SimpleTaggerFile);
        //    // 2) The more advanced tagger: (Does not work)
        //    //SSTagger ssTagger = new SSTagger(Properties.Settings.Default.TaggerDLL);
        //    //for (int i = 0; i <= 15; i++)
        //    //{
        //    //    ssTagger.LoadModelFile(Properties.Settings.Default.SSTaggerModelFile, i);
        //    //}
        //    //tagger = ssTagger;
        //    chunker = new Chunker();
        //    parser = new Proxem.Antelope.Stanford.Parser(Properties.Settings.Default.StanfordParserFile);

        //    lexicon = new Lexicon();
        //    lexicon.LoadDataFromFile(Properties.Settings.Default.LexiconFile, null);
        //}
        public void LoadTagger()
        {
            // There are two tagger types:
            // 1) The following is the simple tagger
            tagger = new SimpleTagger(Properties.Settings.Default.SimpleTaggerFile);
            // 2) The more advanced tagger: (Does not work)
            //SSTagger ssTagger = new SSTagger(Properties.Settings.Default.TaggerDLL);
            //for (int i = 0; i <= 15; i++)
            //{
            //    ssTagger.LoadModelFile(Properties.Settings.Default.SSTaggerModelFile, i);
            //}
            //tagger = ssTagger;
        }
        public void LoadChunker()
        {
            chunker = new Chunker();
        }
        public void LoadLinkGrammarParser()
        {
            linkGrammarParser = new Proxem.Antelope.LinkGrammar.Parser(@"C:\Users\Krikor\Documents\GitHub\ConversationAI\bin", @"C:\Users\Krikor\Documents\GitHub\ConversationAI\data\LinkGrammar"); // Took out the lexicon argument for debugging
        }
        public void LoadStanfordParser()
        {
            stanfordParser = new Proxem.Antelope.Stanford.Parser(@"C:\Users\Krikor\Documents\GitHub\ConversationAI\data\StanfordParser\englishPCFG.ser.gz",0);
        }
        public void LoadLexicon()
        {
            lexicon = new Lexicon();
            lexicon.LoadDataFromFile("C:\\Users\\Krikor\\Documents\\GitHub\\ConversationAI\\data\\Proxem.Lexicon.dat", null);
        }
        public bool StringIsCapitalized(string input)
        {
            return char.IsUpper(input[0]);
        }
        private void SetVerbTenseBasedOnContext(List<Word> sentence, int verbIndex)
        {

        }
        //public void AddWordToDictionary(Word word)
        //{
        //    try
        //    {
        //        stringWordDictionary.Add(word.HumanRendering, word);
        //    }
        //    catch (ArgumentException)
        //    {
        //        main.ChatWindow.AddLineToChatBox("[Dictionary]", "Could not add " + word.HumanRendering + " to dictionary, word is already defined");
        //    }
        //}
        //public Word TranslateWord(string input, bool ignoreCap = false)
        //{
        //    Word wordOutput;
        //    //How to use Dictionary class: http://msdn.microsoft.com/en-us/library/xfhwa508(v=vs.110).aspx
        //    if (ignoreCap == false)
        //    {
        //        if (StringIsCapitalized(input)) //String is capitalized so it is a proper noun
        //            wordOutput = new Word(Words.UNKNOWN, input);
        //        else
        //        {
        //            // This block trys to define string to word based on stringWordDictionary, if it fails, default to unknown.
        //            try
        //            {
        //                wordOutput = stringWordDictionary[input];
        //            }
        //            catch (KeyNotFoundException)
        //            {
        //                wordOutput = new Word(Words.UNKNOWN, input);
        //                main.ChatWindow.DebugMessage("Could not define " + input + ".");
        //            }
        //        }


        //    }
        //    else
        //    {
        //        try
        //        {
        //            wordOutput = stringWordDictionary[input];
        //        }
        //        catch (KeyNotFoundException)
        //        {
        //            wordOutput = new Word(Words.UNKNOWN, input);
        //            main.ChatWindow.DebugMessage("Could not define " + input + ".");
        //        }
        //    }
        //    //word IS capitalized
        //    //mark unknown
        //    //word NOT capitalized
        //    //translate

        //    //ignoreCap=true
        //    //make word lowercase
        //    //translate
        //    return wordOutput;
        //}
        private IWord FindDependencyByType(IList<IDeepDependency> deepDependencies, DeepDependencyType type)
        {
            IDeepDependency aDep = null;
            foreach (IDeepDependency deepDependency in deepDependencies)
            {
                if (deepDependency.DepType == type)
                {
                    aDep = deepDependency;
                    break;
                }
            }
            if (aDep != null)
                return aDep.Dependent;
            else
                return null;
            
        }
        private List<Word> GetWordsFromISentence(ISentence sentence)
        {
            List<Word> words = new List<Word>();
            foreach(IWord iWord in sentence.Words)
            {
                Word word = new Word(iWord.Text, iWord.Tag, iWord.TagAsString, iWord.PartOfSpeech, iWord.IndexInSentence);
                words.Add(word);
            }
            return words;
        }
        public Sentence TranslateSentence(string input, ParserType parserType) // This is designed to handle simple sentences. Modification is necessary for compound/complex
        {
            IParser parser;
            if(parserType == ParserType.LinkGrammar)
            {
                parser = linkGrammarParser;
            }
            else if(parserType == ParserType.Stanford)
            {
                parser = stanfordParser;
            }
            else
            {
                main.ChatWindow.DebugMessage("Could not find requested parser, defaulting to Stanford Parser...");
                parser = stanfordParser;
            }
            string text = input;
            //Must Define all 7 variables by processing input
            IList<Word> words;
            IAnalysis analysis;
            IList<IChunk> chunks;
            IList<IDeepDependency> dependencies;
            ISyntacticNode rootNode;
            IList<IToken> tokens;
            Sentence.SentenceTypes sentenceType;
            
            // Tokenize and tag the sentence
            IList<IWord> taggedWords = tagger.TagText(input);
            // Collapse the sentece based on collocations (words that are better grouped, Ex. "I like using Visual Studio" Visual Studio does not make sense split apart.
            tagger.CollapseCollocations(taggedWords, lexicon);
            // (1/7) Chunk the words together and define chunks
            chunks = chunker.ChunkText(taggedWords);
            
            // Create a parsed sentence

            ISentence parsedSentence = parser.ParseSentence(input);
            // (2/7) Define dependencies
            dependencies = parsedSentence.DeepSyntaxDependencies;
            // To handle special cases with no dependencies:
            // (So far the only case which has no dependencies is the implied "you" case. So just add a "you" and move on. )
            if (dependencies.Count < 1)             
            {
            }
            else
            {
                parsedSentence = parser.ParseSentence(input);

            }
            // (3/7) Analysis
            analysis = parsedSentence.BestAnalysis;
            // (4/7) Root
            rootNode = parsedSentence.Root;
            // (5/7) Tokens
            tokens = parsedSentence.Tokens;
            // (6/7) Words
            words = GetWordsFromISentence(parsedSentence);
            // (7/7) Sentence Types
            //Not Worked out how to do this yet..
            sentenceType = Sentence.SentenceTypes.Unknown;
                    //foreach(IDeepDependency dependency in dependencies)
                    //{
                    //    switch(dependency.DepType)
                    //    {
                    //        case DeepDependencyType.Subject:
                    //            dependencyText = "Subject: " + dependency.Dependent.Text;
                    //            break;
                    //        case DeepDependencyType.DirectObject:
                    //            dependencyText = "Direct Object: " + dependency.Dependent.Text;
                    //            break;
                    //        case DeepDependencyType.IndirectObject:
                    //            dependencyText = "Indirect Object: " + dependency.Dependent.Text;
                    //            break;
                    //        case DeepDependencyType.AdjectiveNoun:
                    //            dependencyText = "Adjective Noun: " + dependency.Dependent.Text;
                    //            break;
                    //        case DeepDependencyType.NounOfNoun:
                    //            dependencyText = "Noun of Noun: " + dependency.Dependent.Text;
                    //            break;
                    //        case DeepDependencyType.PrepObject:
                    //            dependencyText = "Prep Object: " + dependency.Dependent.Text;
                    //            break;
                    //        case DeepDependencyType.SpaceComplement:
                    //            dependencyText = "Space Complement: " + dependency.Dependent.Text;
                    //            break;
                    //        case DeepDependencyType.TimeComplement:
                    //            dependencyText = "Space Complement: " + dependency.Dependent.Text;
                    //            break;
                    //    }
                    //    governorText = "Governor: " + dependency.Governor.Text;
                    //    main.ChatWindow.DebugMessage(dependencyText + governorText);
                    //}
            return new Sentence(text,words,analysis,chunks,dependencies,rootNode,tokens,sentenceType);
            // obsolete/defunct Old Code vvv
            //main.ChatWindow.DebugMessage(outputSentence);
            //// Generate the variables with empty values. We will Fill these in through the translation process.
            //int sentenceType = Sentence.UNKNOWN; // This is going to be difficult...
            //List<Word> computerRenderList = new List<Word>(); // Got it
            //int subjectIndex = -1; // Figure this out  
            //int mainVerbIndex = -1; // 
            //int punctuation = -1;

            //// Commence the fun stuff!
            //string[] splitString = input.Split(' ');

            //// the first letter of the sentence is capitalized, we don't need that.
            //splitString[0] = splitString[0].ToLower();

            //// remove the punctuation and add it as a property to the sentence object
            //// to get punctuation symbol, get last letter of last word of the array.
            //int lastWordIndex = splitString.Length - 1;
            //char punctuationSymbol = splitString[lastWordIndex][splitString[lastWordIndex].Length - 1];
            //// assign the punctuation variable and prevent removal of punctuation if it does not exist
            //bool punctuationExists = true;
            //punctuation = Sentence.NONE;
            //switch (punctuationSymbol)
            //{
            //    case '.':
            //        punctuation = Sentence.PERIOD;
            //        break;
            //    case '?':
            //        punctuation = Sentence.QUESTION_MARK;
            //        break;
            //    case '!':
            //        punctuation = Sentence.EXCLAMATION_MARK;
            //        break;
            //    default:
            //        punctuationExists = false;
            //        break;
            //}
            //if (punctuationExists)
            //{
            //    splitString[lastWordIndex] = splitString[lastWordIndex].Substring(0, splitString[lastWordIndex].Length - 1);
            //}

            //// Go through the sentence and convert it to Word format.
            //int currentIndex = 0;
            //foreach (string stringWord in splitString)
            //{
            //    Word word;
            //    word = TranslateWord(stringWord);
            //    computerRenderList.Add(word);
            //    currentIndex++;
            //}
            //currentIndex = 0;
            //foreach (Word word in computerRenderList)
            //{
            //    if (word.Type == Word.VERB)
            //    {
            //        //SetVerbTenseBasedOnContext(computerRenderList,)
            //    }
            //    currentIndex++;
            //}

            //// translation code here. take the input and simplify it.
            //Sentence sentence = new Sentence(sentenceType, computerRenderList, subjectIndex, mainVerbIndex, punctuation, chunks);
            //return sentence;
        }
    }
}
