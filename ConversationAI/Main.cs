﻿using Proxem.Antelope.Lexicon;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Windows.Media;

namespace ConversationAI
{
    class Main
    {
        /*
         * Constant
         */
        public const bool DEBUG = true;

        /*
         * Variables
         */
        public Robot robot;
        public bool finishedLoading = false;

        // For the BackgroundWorker
        private Run loadingRun;
        public SolidColorBrush loadingCompleteColor = new SolidColorBrush();
        public SolidColorBrush loadingStartedColor = new SolidColorBrush();

        /*
         * Properties
         */
        // Auto-implemented properties
        public ChatWindow ChatWindow { get; set; }
        public Rules Rules { get; set; }
        public Translator Translator { get; set; }
        


        /*
         * Constructor
         */
        public Main(string name, int robotType, int assertiveness, int factuality, int emotion, int organization, MainWindow mainWindow)
        {
            // Configure the robot variable
            CreateRobot(name, robotType, assertiveness, factuality, emotion, organization);
            ChatWindow = new ChatWindow(robot);
            ChatWindow.Title = robot.Name;
            ChatWindow.Show();
            mainWindow.Close();

            // Background Worker
            BackgroundWorker backgroundWorker = new BackgroundWorker();
            backgroundWorker.WorkerSupportsCancellation = true;
            backgroundWorker.WorkerReportsProgress = true;

            backgroundWorker.DoWork += new DoWorkEventHandler(backgroundWorker_DoWork);
            backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker_ProgressChanged);
            backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker_RunWorkerCompleted);
            if (backgroundWorker.IsBusy != true)
                backgroundWorker.RunWorkerAsync();

            loadingCompleteColor = Brushes.Black;
            loadingStartedColor = Brushes.Gainsboro;

        }
        
        /*
         * Methods
         */
        public void CreateRobot(string name, int robotType, int assertiveness, int factuality, int emotion, int organization)
        {
            // This is where all the robot creation is sorted out. ADD NEW ROBOTS HERE!
            if(robotType == RobotTypes.TASKER)
            {
                robot = new RobotTasker(this, name, robotType, assertiveness, factuality, emotion, organization);
            }
        }

        /*
         * Event Handlers
         */
        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            ProgressManager progressManager = new ProgressManager();
            
            worker.ReportProgress(0, "Translator Tools > Loading...");
            Translator = new Translator(this);
            worker.ReportProgress(20, "Translator Tools > Loading Chunker...");
            Translator.LoadChunker();
            worker.ReportProgress(40, "Translator Tools > Loading Lexicon...");
            Translator.LoadLexicon();
            worker.ReportProgress(60, "Translator Tools > Loading Parser...");
            Translator.LoadLinkGrammarParser();
            Translator.LoadStanfordParser();
            worker.ReportProgress(80, "Translator Tools > Loading Tagger...");
            Translator.LoadTagger();
            worker.ReportProgress(100, "Translator Tools > Done.");
            worker.ReportProgress(0, "Rule Files > Loading...");
            Rules = new Rules(this);
            worker.ReportProgress(100, "Rule Files > Done.");
            
        }
        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //this.tbProgress.Text = (e.ProgressPercentage.ToString() + "%");
            if(e.ProgressPercentage == 0)
            {
                loadingRun = ChatWindow.AddRunWithInstance();
                loadingRun.Text = e.UserState.ToString();
                loadingRun.Foreground = loadingStartedColor;
            }
            else if(e.ProgressPercentage == 100)
            {
                loadingRun.Text = e.UserState.ToString();
                loadingRun.Foreground = loadingCompleteColor;
            }
            else
            {
               double p = e.ProgressPercentage;
                loadingRun.Text = e.UserState.ToString();
                double R, G, B;
                //if (System.Convert.ToInt32(loadingStartedColor.Color.R) < System.Convert.ToInt32(loadingCompleteColor.Color.R))
                //{ 
                    R = (1 - p / 100) * System.Convert.ToInt32(loadingStartedColor.Color.R) + (p / 100) * System.Convert.ToInt32(loadingCompleteColor.Color.R); 
                //}
                //else
                    //{ R= (1 - p / 100) * System.Convert.ToInt32(loadingCompleteColor.Color.R) + (p / 100) * System.Convert.ToInt32(loadingStartedColor.Color.R); }
                //if (System.Convert.ToInt32(loadingStartedColor.Color.G) < System.Convert.ToInt32(loadingCompleteColor.Color.G))
                    //{ 
                        G = (1 - p / 100) * System.Convert.ToInt32(loadingStartedColor.Color.G) + (p / 100) * System.Convert.ToInt32(loadingCompleteColor.Color.G); 
                    //}
                //else
                    //{ 
                        //G = (1 - p / 100) * System.Convert.ToInt32(loadingCompleteColor.Color.G) + (p / 100) * System.Convert.ToInt32(loadingStartedColor.Color.G); 
                    //}
                //if (System.Convert.ToInt32(loadingStartedColor.Color.B) < System.Convert.ToInt32(loadingCompleteColor.Color.B))
                   // { 
                        B = (1 - p / 100) * System.Convert.ToInt32(loadingStartedColor.Color.B) + (p / 100) * System.Convert.ToInt32(loadingCompleteColor.Color.B); 
                    //}
                //else

                   // { B = (1 - p / 100) * System.Convert.ToInt32(loadingCompleteColor.Color.B) + (p / 100) * System.Convert.ToInt32(loadingStartedColor.Color.B); }
                Color color = Color.FromRgb(System.Convert.ToByte(R), System.Convert.ToByte(G), System.Convert.ToByte(B));
                SolidColorBrush gradientColor = new SolidColorBrush(color);
                loadingRun.Foreground = gradientColor;
            }
        }
        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if ((e.Cancelled == true))
            {
                //this.tbProgress.Text = "Canceled!";
            }

            else if (!(e.Error == null))
            {
                //this.tbProgress.Text = ("Error: " + e.Error.Message);
            }

            else
            {
                finishedLoading = true;
                ChatWindow.sendButton.IsEnabled = true;
            }
        }
    }
}
