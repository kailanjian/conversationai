﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace ConversationAI
{
    class Rules
    {
        // CONSTANTS
        public const int DICTIONARY = 0;
        public const int RULESET = 1;
        public const int KNOWLEDGESET = 2;
        // Variables
        public string currentDirectory = Environment.CurrentDirectory;
        string[] ruleFiles = { "dictionary.txt", "etiquette.txt", "knowledge.txt" };
        Main main;

        //Properties

        //Constructor
        public Rules(Main main)
        {
            this.main = main;
            ConfigureDirectory(currentDirectory);
        }
        //Methods
        public void ConfigureDirectory(string directory) //Search for files, creates files if not found
        {
            foreach(string file in ruleFiles)
            {
                if(File.Exists(directory + "\\" + file))
                {

                }
                else
                {
                    //*Removed for thread safety* main.ChatWindow.SystemMessage("Could not find file: " + file + " in directory: " + directory + " Creating(Fill this in)...");
                    File.Create(directory + "\\" + file);

                }
            }
            // Process files here
            //*Removed for thread safety* main.ChatWindow.SystemMessage("Loading Dictionaries...");
            LoadFile("dictionary.txt",DICTIONARY); // load dictionary.txt
            LoadFile(ruleFiles[2],KNOWLEDGESET); // Load knowledge.txt
        }
        private void LoadFile(string fileName, int mode)
        {
            if (mode == DICTIONARY)
            {
                //LoadDictionary(fileName);
            }
            else if(mode == RULESET)
            {

            }
            else if(mode == KNOWLEDGESET)
            {
                //LoadKnowledgeSet(fileName);
            }
        }
        //private void LoadDictionary(string fileName)
        //{
        //    string directory = currentDirectory + "\\" + fileName;
        //    int counter = 0;
        //    string line;
        //    try
        //    {
        //        using (StreamReader reader = new StreamReader(directory))
        //        {
        //            //Idea for future: make a comment line system, skip over lines that begin with # or other symbol.
        //            //LEGEND:
        //            //this loop processes lines with the following standard of organization
        //            // WORDNUM(int)|SIGN(string: "+" or "-")|MODIFIER(int)|TYPE(string: "noun", "pronoun", etc.)|ENGLISHWORD(string)
        //            while ((line = reader.ReadLine()) != null)
        //            {
        //                if(line.StartsWith("##"))
        //                {
        //                    continue;
        //                }
        //                counter++;
        //                string[] arguments = line.Split('|'); //split into 5 pieces
        //                int computerRendering = Convert.ToInt32(arguments[0]);
        //                int state = 2;
        //                if (arguments[1].Equals("+"))
        //                    state = 1;
        //                else if (arguments[1].Equals("0"))
        //                    state = 0;
        //                else if (arguments[1].Equals("-"))
        //                    state = -1;
        //                //*Removed for thread safety* else
        //                    //*Removed for thread safety* main.ChatWindow.SystemMessage("Error processing type in rule file: " + fileName + " on line " + counter + " please use \"+\", \"0\", or \"-\" ");
        //                int modifier = Convert.ToInt32(arguments[2]);
        //                int type = 0;
        //                switch (arguments[3].ToLower())
        //                {
        //                    case "noun":
        //                        type = 1;
        //                        break;
        //                    case "pronoun":
        //                        type = 2;
        //                        break;
        //                    case "adjective":
        //                        type = 3;
        //                        break;
        //                    case "verb":
        //                        type = 4;
        //                        break;
        //                    case "adverb":
        //                        type = 5;
        //                        break;
        //                    case "preposition":
        //                        type = 6;
        //                        break;
        //                    case "conjunction":
        //                        type = 7;
        //                        break;
        //                    case "interjection":
        //                        type = 8;
        //                        break;
        //                    default:
        //                        //*Removed for thread safety* main.ChatWindow.SystemMessage("Error processing type in rule file " + fileName + " on line " + counter);
        //                        break;

        //                }
        //                string humanRendering = arguments[4];
        //                Word word = new Word(computerRendering, humanRendering, state, modifier, type);
        //                main.Translator.AddWordToDictionary(word);
        //            }   // End of loop to add a single word.
        //        }   // Finished adding all the words, using statement automatically closes the stream.
        //        // Finished the using statement without breaking so we are good! Success message vvv
        //        //*Removed for thread safety* main.ChatWindow.SystemMessage("Finished. Loaded " + counter + " words.");
        //    }
        //    catch(Exception e)
        //    {
        //        //*Removed for thread safety* main.ChatWindow.SystemMessage("Error in dictionary file at line " + counter + ". Could not finish reading.");
        //    }
        //    //Idea for future: figure out how to make the loop skip over error inducing lines.
        //}// End of LoadDictionary method
        //private void LoadKnowledgeSet(string fileName)
        //{
        //    string directory = currentDirectory + "\\" + fileName;
        //    int counter = 0;
        //    string line;
        //    try
        //    {
        //        using (StreamReader reader = new StreamReader(directory))
        //        {
        //            //Idea for future: make a comment line system, skip over lines that begin with # or other symbol.
        //            //LEGEND:
        //            //this loop processes lines with the following standard of organization
        //            // WORDNUM(int)|SIGN(string: "+" or "-")|MODIFIER(int)|TYPE(string: "noun", "pronoun", etc.)|ENGLISHWORD(string)
        //            while ((line = reader.ReadLine()) != null)
        //            {
        //                if(line.StartsWith("##"))
        //                {
        //                    continue;
        //                }
        //                counter++;
        //                string[] arguments = line.Split('|'); //split into 5 pieces
        //                int computerRendering = Convert.ToInt32(arguments[0]);
        //                int state = 2;
        //                if (arguments[1].Equals("+"))
        //                    state = 1;
        //                else if (arguments[1].Equals("0"))
        //                    state = 0;
        //                else if (arguments[1].Equals("-"))
        //                    state = -1;
        //                //*Removed for thread safety* else
        //                   //*Removed for thread safety*  main.ChatWindow.SystemMessage("Error processing type in rule file: " + fileName + " on line " + counter + " please use \"+\", \"0\", or \"-\" ");
        //                int modifier = Convert.ToInt32(arguments[2]);
        //                int type = 0;
        //                switch (arguments[3].ToLower())
        //                {
        //                    case "noun":
        //                        type = 1;
        //                        break;
        //                    case "pronoun":
        //                        type = 2;
        //                        break;
        //                    case "adjective":
        //                        type = 3;
        //                        break;
        //                    case "verb":
        //                        type = 4;
        //                        break;
        //                    case "adverb":
        //                        type = 5;
        //                        break;
        //                    case "preposition":
        //                        type = 6;
        //                        break;
        //                    case "conjunction":
        //                        type = 7;
        //                        break;
        //                    case "interjection":
        //                        type = 8;
        //                        break;
        //                    default:
        //                        main.ChatWindow.SystemMessage("Error processing type in rule file " + fileName + " on line " + counter);
        //                        break;

        //                }
        //                string humanRendering = arguments[4];
        //                Word word = new Word(computerRendering, humanRendering, state, modifier, type);
        //                main.Translator.AddWordToDictionary(word);
        //            }   // End of loop to add a single word.
        //        }   // Finished adding all the words, using statement automatically closes the stream.
        //        // Finished the using statement without breaking so we are good! Success message vvv
        //        //*Removed for thread safety* main.ChatWindow.SystemMessage("Finished. Loaded " + counter + " words.");
        //    }
        //    catch (Exception e)
        //    {
        //        //*Removed for thread safety* main.ChatWindow.SystemMessage("Error in dictionary file at line " + counter + ". Could not finish reading.");
        //    }
        //    //Idea for future: figure out how to make the loop skip over error inducing lines.
        //}// End of LoadKnowledgeSet method
    }// END CLASS
}// END NAMESPACE
