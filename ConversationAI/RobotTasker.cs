﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Proxem.Antelope.Parsing;
using Proxem.Antelope.Tagging;
using System.Windows.Media;

namespace ConversationAI
{
    class RobotTasker : Robot
    {
        //Variables

        private System.Windows.Media.SolidColorBrush color = System.Windows.Media.Brushes.MediumVioletRed;
        
        Main main;
        private SolidColorBrush nameColor = Brushes.CornflowerBlue;
        // Properties

        //Constructor
        public RobotTasker(Main main, string name, int robotType, int assertiveness, int factuality, int emotion, int organization)
            : base(name, robotType, assertiveness, factuality, emotion, organization)
        {
            this.main = main;
        }
        //Methods
        public override void ProcessInput(string input)
        {
            // Old code using dependencies (doesn't work well)
            //// So far it only handles simple sentences...
            //Sentence sentence = main.Translator.TranslateSentence(input);
            //if (sentence.Dependencies.Count < 1)
            //{
            //    main.ChatWindow.AddLineToChatBox(Name, "I can't find any dependencies!");
            //}
            //else
            //{
            //    SendMessage("Here are the dependencies I found:");
            //    Word verb = null;
            //    foreach(IDeepDependency dependency in sentence.Dependencies)
            //    {
            //        SendMessage(dependency.DepType.ToString() + ": " + dependency.Dependent.Text + ", Governor: " + dependency.Governor);
            //        if(dependency.DepType == DeepDependencyType.Subject || dependency.DepType == DeepDependencyType.DirectObject)
            //        {
            //            verb = new Word(dependency.Governor);
            //        }
            //    }
            //    if (verb != null)
            //    {
            //        string output = "Verb with modals: ";
            //        foreach(Word word in SenteceUtils.FindModalsForVerb(verb,sentence))
            //        {
            //            output = output + word.Text + " ";
            //        }
            //        output = output + verb.Text;
            //        SendMessage(output);
            //    }

            //}
            //if(FindCommand(sentence))
            //{
            //    // Run the command
            //}

            Sentence sentence;
            if ((sentence = main.Translator.TranslateSentence(input, Translator.ParserType.Stanford)) == null)
            {
                sentence = main.Translator.TranslateSentence(input, Translator.ParserType.LinkGrammar);
            }
            StringBuilder mainSentence = new StringBuilder();
            foreach (IChunk chunk in sentence.Chunks)
            {
                if (chunk.Type != SyntacticNodeType.PP)
                {
                    foreach (var word in chunk.Words)
                    {
                        mainSentence.Append(word.Text);
                    }
                }
            }
            
            Word mainVerb = SenteceUtils.GetMainVerb(sentence.NodeTree.RootNode);
            if (mainVerb == null)
            {
                sentence = main.Translator.TranslateSentence(input, Translator.ParserType.LinkGrammar);
                mainVerb = SenteceUtils.GetMainVerb(sentence.NodeTree.RootNode);
            }
            if (mainVerb != null)
            {
                SendMessage(mainVerb.Text);
                Node verbNode = SenteceUtils.GetNodeOfWord(mainVerb, sentence.NodeTree.RootNode);
            }
            else
                SendMessage("I could not find a verb in this sentence!");

            SendMessage(SenteceUtils.GetDirectObject(mainVerb, sentence)); 
        }
        private void SendMessage(string message)
        {
            main.ChatWindow.AddLineToChatBox(Name, message, nameColor);
        }
        private bool FindCommand(Sentence sentence)
        {
            bool commandFound = false;
            return commandFound;
        }

    }
}
