﻿using Proxem.Antelope;
using Proxem.Antelope.Tagging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ConversationAI
{
    class Word
    {
        // Constants
        public const int PLUS = 1;
        public const int STANDARD = 0; // These 3 serve as toggles.
        public const int MINUS = -1;

        public const int UNKNOWN = 0;
        public const int NOUN = 1;
        public const int PRONOUN = 2;
        public const int ADJECTIVE = 3;
        public const int VERB = 4;
        public const int ADVERB = 5;
        public const int PREPOSITION = 6;
        public const int CONJUNCTION = 7;
        public const int INTERJECTION = 8;

        // Variables
        //private string humanRendering = "ND";
        //private int computerRendering = Words.UNKNOWN; // I'm calling the actual word itself "content" for the Word class to not get mixed up
        //private int state = STANDARD; // Categorizes the word for translation. FEELING+ could be happy FEELING could be OK FEELING- bad.
        //private int modifier = 0; // Modifier just in case there is more than 3 states. "Great" could be FEELING+[1] 
        //private int type = 0;
        //private string tagString;
        // Variables
        //Properties
        // Auto-Implemented
        public int IndexInSentence { get; set; }
        public PartOfSpeech Pos{ get; set; }
        public TagType Tag { get; set; }
        public string TagAsString { get; set; }
        public string Text { get; set; }
        
        //Properties
        //public int ComputerRendering { get; set;} // Actual string
        //public int State { get; set; }
        //public int Modifier { get; set; }
        //public int Type { get; set; }
        //public string HumanRendering { get; set; }
        //public string TagString { get; set; }
        //Constructor
        /// <summary>
        /// Construct new word
        /// </summary>
        public Word(string text, TagType tag, string tagAsString, PartOfSpeech pos, int indexInSentence)
        {
            IndexInSentence = indexInSentence;
            Text = text;
            Tag = tag;
            TagAsString = tagAsString;
            Pos = pos;
        }
        /// <summary>
        /// Create word from IWord
        /// </summary>
        public Word(IWord word)
        {
            IndexInSentence = word.IndexInSentence;
            Text = word.Text;
            Tag = word.Tag;
            TagAsString = word.TagAsString;
            Pos = word.PartOfSpeech;
        }
        public override string ToString()
        {
            return this.Text;
        }
    }
}
