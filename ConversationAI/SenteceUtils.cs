﻿using Proxem.Antelope.Lexicon;
using Proxem.Antelope.Parsing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Proxem.Antelope.Tagging;

namespace ConversationAI
{
    class SenteceUtils
    {
        /// <summary>
        /// Find the node corresponding to the word.
        /// </summary>
        /// <param name="word">Word to search for</param>
        /// <param name="rootNode">Root node of tree which will be searched</param>
        /// <returns>Returns the node which has a matching leaf or null if not found</returns>
        public static Node GetNodeOfWord(Word word, Node rootNode)
        {
            // Recursively goes through tree, for each node, check if node has word then run method on children.
            if (rootNode.Leaf != null && rootNode.Leaf.Equals(word))
            {
                return rootNode;
            }
            if (rootNode.Children != null)
            {
                foreach (var node in rootNode.Children)
                {
                    if (GetNodeOfWord(word, node) != null)
                    {
                        return GetNodeOfWord(word, node);
                    }
                }
            }
            return null;
        }
        private static bool IsVerb(Word word)
        {
            return word.TagAsString.StartsWith("VB");
        }
        ///<summary>
        /// Find the modal that applies to the verb
        ///</summary>
        public static IList<Word> FindModalsForVerb(Word verb, Sentence sentence)
        {
            IList<Word> modals = new List<Word>();
            // A simple process, get index of verb search for modal before
            int verbIndex = verb.IndexInSentence;
            foreach (Word word in sentence.Words)
            {
                if (word.IndexInSentence >= verb.IndexInSentence) break;
                if (word.Tag == Proxem.Antelope.Tagging.TagType.EnglishMD)
                {
                    modals.Add(word);
                }
            }
            return modals;
        }
        /// <summary>
        /// A debugging method to spit out the contents of the ISyntacticNode tree recursively
        /// </summary>
        /// <param name="rootNode">Starting node</param>
        /// <param name="nodeContents">A list in which to store all the node information</param>
        public static void FindStringsRecursively(Node rootNode, StringBuilder nodeContents)
        {
            if (rootNode.Leaf != null)
            {
                nodeContents.Append(rootNode.Leaf.Text);
            }
            if (rootNode.Children != null)
            {
                foreach (Node node in rootNode.Children)
                {
                    FindStringsRecursively(node, nodeContents);
                }
            }
        }

        public static void DeleteNodesOfType(Node rootNode, SyntacticNodeType nodeType)
        {
            if (rootNode.Leaf != null && rootNode.Type == nodeType)
            {
                rootNode.Parent.Children.Remove(rootNode); 
            }
            if (rootNode.Children != null)
            {
                foreach (var node in rootNode.Children)
                {
                    DeleteNodesOfType(node, nodeType);
                }
            }
        }
        public static Node FindParentOfType(SyntacticNodeType nodeType, Node startNode)
        {
            if (startNode.Parent != null && startNode.Parent.Type == nodeType)
            {
                return startNode.Parent;
            }
            FindParentOfType(nodeType, startNode.Parent);
            return null;
        }
        public static Word GetMainVerb(Node rootNode)
        {
            if (rootNode.Leaf != null && IsVerb(rootNode.Leaf))
            {
                return rootNode.Leaf;
            }
            if (rootNode.Children != null)
            {
                foreach (var node in rootNode.Children)
                {
                    if (GetMainVerb(node) != null)
                    {
                        return GetMainVerb(node);
                    }
                }
            }
            return null;
        }

        public static string GetDirectObject(Word verb, Sentence sentence)
        {
            Node verbNode = GetNodeOfWord(verb,sentence.NodeTree.RootNode);
            List<Node> siblings = verbNode.Parent.Children;
            var itemToRemove = siblings.SingleOrDefault(r => r.Equals(verbNode));
            if (itemToRemove != null)
                siblings.Remove(itemToRemove);
            string result = "";
            foreach (var sibling in siblings)
            {
                DeleteNodesOfType(sibling, SyntacticNodeType.PP); // Prepositional phrases should be removed.
                StringBuilder siblingContents = new StringBuilder();
                FindStringsRecursively(sibling, siblingContents);
                result = String.Concat(result, siblingContents);
            }
            return result;
        }
    }
}
